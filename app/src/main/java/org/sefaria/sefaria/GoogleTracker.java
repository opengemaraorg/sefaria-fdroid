package org.sefaria.sefaria;

/**
 * Created by LenJ on 2/4/2016.
 */

import android.content.SharedPreferences;


import java.util.Random;

public class GoogleTracker extends MyApp {

    private static final String GOOGLE_AN_ID = "UA-73355210-1";
    public static String randomID = null;
    public static final String CATEGORY_NEW_TEXT = "Opened Segment Page";
    public static final String BUTTON_PRESS = "Button Press";
    public static final String SETTING_CHANGE = "Setting Change";
    public static final String CATEGORY_OPEN_MENU = "Opened Menu page";
    public static final String CATEGORY_SCREEN_CHANGE = "Screen Change";
    public static final String CATEGORY_STATUS_INFO = "Status Info";
    public static final String CATEGORY_OPENED_URL = "Opened URL";
    public static final String CATEGORY_OPEN_NEW_BOOK_ACTION = "Open new book action";
    public static final String CATEGORY_OPEN_NEW_BOOK_ACTION_2 = "Open new book action 2";
    public static final String CATEGORY_API_REQUEST = "API request";
    public static final String CATEGORY_RANDOM_ERROR = "Random Error";
    public static final String CATEGORY_FIND_ON_PAGE = "Find on Page";


    public GoogleTracker(){
        getTracker();
        setTrackerID();
        sendEvent(CATEGORY_SCREEN_CHANGE, "Started App");
        sendEvent("Menu lang",Settings.lang2Str(Settings.getDefaultTextLang()));
        int theme = Settings.getTheme();
        String themeName = "";
        if(theme == R.style.SefariaTheme_White)
            themeName = "SefariaTheme_White";
        else if(theme == R.style.SefariaTheme_Grey)
            themeName = "SefariaTheme_Grey";
        else if(theme == R.style.SefariaTheme_Black)
            themeName = "SefariaTheme_Black";
        sendEvent("Theme",themeName);
        Boolean sideBySide = Settings.getIsSideBySide();
        sendEvent("sideBySide", sideBySide.toString());
        sendEvent("Segment lang",Settings.lang2Str(Settings.getDefaultTextLang()));
    }


    /*private static void setTrackerID() {};
    synchronized  void getTracker() {};
    static public void sendEvent(String cat, String act, long value) {};
    static public void sendEvent(String cat, String act) {};
    static public void sendScreen(String screenName){};
    static public void sendException(Exception e){};
    static public void sendException(Exception e, String addedText){};*/


    private static void setTrackerID(){
        if(randomID == null){
            SharedPreferences settings = Settings.getGeneralSettings();
            randomID = settings.getString("randomID","");
            if(randomID.equals("")){
                Random random = new Random();
                Long longID = random.nextLong(); //there's a really small chance that it's 0... we're going to ignore that.
                randomID = "" + longID;
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("randomID",randomID);
                editor.apply();
            }
        }

        try{

        } catch (Exception e){
            e.printStackTrace();
            sendException(e);
        }
    }

    synchronized void getTracker() {
    }

    static public void sendEvent(String cat, String act, long value){

    }

    static public void sendEvent(String cat, String act){

    }



    static public void sendScreen(String screenName){


    }


    static public void sendException(Exception e){

    }

    static public void sendException(Exception e, String addedText){


    }



}
